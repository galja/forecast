package com.weather.forecast.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CountryDTO {
        private Integer countryId;
        private String countryName;
}
