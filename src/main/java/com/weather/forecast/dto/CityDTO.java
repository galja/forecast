package com.weather.forecast.dto;

import com.weather.forecast.model.Country;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.Id;

@Data
@RequiredArgsConstructor
public class CityDTO {
    @Id
    private Integer cityId;
    private String name;
    private Country country;
    private Integer timezone;
}
