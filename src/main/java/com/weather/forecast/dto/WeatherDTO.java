package com.weather.forecast.dto;
import com.weather.forecast.model.City;

import javax.persistence.Id;

public class WeatherDTO {
    @Id
    private Integer id;
    private Double temperature;
    private Double humidity;
    private Double windSpeed;
    private Double cloudiness;
    private City city;
}
