package com.weather.forecast.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.weather.forecast.model.*;

import java.io.IOException;

public class Deserializer extends StdDeserializer<Weather> {

    public Deserializer() {
        this(Weather.class);
    }

    private Deserializer(Class<?> classToDeserialize) {
        super(classToDeserialize);
    }

    @Override
    public Weather deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        TreeNode treeNode = jsonParser.readValueAsTree();

        Double temperature = Double.parseDouble(treeNode.get("main").get("temp").toString());
        Double humidity = Double.parseDouble(treeNode.get("main").get("humidity").toString());
        Double windSpeed = Double.parseDouble(treeNode.get("wind").get("speed").toString());
        Double cloudiness = Double.parseDouble(treeNode.get("clouds").get("all").toString());
        return new Weather(temperature, humidity, windSpeed, cloudiness);
    }
}
