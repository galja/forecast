package com.weather.forecast.controller;

import java.util.List;
import com.weather.forecast.model.Country;
import com.weather.forecast.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CountryController {

    private final CountryService countryService;

    @Autowired
    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping("/country")
    public List<Country> getAllCountries(){
        return countryService.getAllCountries();
    }

    @PostMapping("/country")
    public Country addCountry(@Valid @RequestBody Country country){
        return countryService.addCountry(country);
    }

    @PutMapping("/country/{countryId}")
    public Country updateCountry(@PathVariable Integer countryId, @Valid @RequestBody Country country){
        return countryService.updateCountry(countryId, country);
    }
}
