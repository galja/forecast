package com.weather.forecast.controller;

import com.weather.forecast.model.City;
import com.weather.forecast.repository.CityRepository;
import com.weather.forecast.repository.CountryRepository;
import com.weather.forecast.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@RestController
public class CityController {

    private final CityService cityService;

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping("/cities")
    public Page<City> getAllCities(Pageable pageable) {
        return cityService.getAllCities(pageable);
    }

//    @GetMapping("/countries/{countryId}/cities")
//    public Page<City> getAllCitiesByCountryId(Pageable pageable, @PathVariable Integer countryId) {
//        return cityRepository.findAllCitiesByCountryId(countryId, pageable);
//    }

    @PostMapping("/countries/{countryId}/cities")
    public City addCity(@PathVariable Integer countryId, @Valid @RequestBody City city) {
        return cityService.addCity(countryId, city);
    }

    @PutMapping("countries/{countryId}/cities/{cityId}")
    public City updateCity(@PathVariable Integer countryId, @PathVariable Integer cityId, @Valid @RequestBody City city) {
        return cityService.updateCity(countryId, cityId, city);
    }
}
