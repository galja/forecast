package com.weather.forecast.service;

import com.weather.forecast.model.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CityService {
    Page<City> getAllCities(Pageable pageable);

    City addCity(Integer countryId, City city);

    City updateCity(Integer countryId, Integer cityId, City city);
}
