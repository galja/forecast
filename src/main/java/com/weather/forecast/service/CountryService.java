package com.weather.forecast.service;

import com.weather.forecast.model.Country;

import java.util.List;


public interface CountryService {
    List<Country> getAllCountries();
    Country addCountry(Country country);
    Country updateCountry(Integer countryId, Country country);
}
