package com.weather.forecast.service.implementation;

import com.weather.forecast.model.City;
import com.weather.forecast.repository.CityRepository;
import com.weather.forecast.repository.CountryRepository;
import com.weather.forecast.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@Service
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;
    private final CountryRepository countryRepository;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository, CountryRepository countryRepository) {
        this.cityRepository = cityRepository;
        this.countryRepository = countryRepository;
    }

    @Override
    public Page<City> getAllCities(Pageable pageable) {
        return cityRepository.findAll(pageable);
    }

    public City addCity(Integer countryId, City city) {
        return countryRepository.findById(countryId)
                .map(country -> {
                    {
                        city.setCountry(country);
                        return city;
                    }
                }).orElseThrow(EntityNotFoundException::new);
    }

    public City updateCity(Integer countryId, Integer cityId, City city) {
        if (!countryRepository.existsById(countryId)) {
            throw new EntityNotFoundException("Country not found");
        }
        return cityRepository.findById(cityId)
                .map(s -> cityRepository.save(updateFields(s, city)))
                .orElseThrow(() -> new EntityNotFoundException("City not found"));
    }

    private City updateFields(City city, City updatedCity) {
        city.setName(updatedCity.getName());
        city.setCountry(updatedCity.getCountry());
        city.setTimezone(updatedCity.getTimezone());
        return city;
    }
}
