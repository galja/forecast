package com.weather.forecast.service.implementation;

import java.util.List;
import com.weather.forecast.model.Country;
import com.weather.forecast.repository.CountryRepository;
import com.weather.forecast.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> getAllCountries(){
        return countryRepository.findAll();
    }

    @Override
    public Country addCountry(Country country){
        return countryRepository.save(country);
    }

    @Override
    public Country updateCountry(Integer countryId, Country country){
        return countryRepository.findById(countryId)
                .map(s -> countryRepository.save(updateFields(s,country)))
                .orElseThrow(()->new EntityNotFoundException("Not found"));
    }

    private Country updateFields(Country country, Country updatedCountry){
        country.setCountryName(updatedCountry.getCountryName());
        return country;
    }
}
