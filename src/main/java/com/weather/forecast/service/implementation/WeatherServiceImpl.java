package com.weather.forecast.service.implementation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.weather.forecast.deserializer.Deserializer;
import com.weather.forecast.model.City;
import com.weather.forecast.model.Country;
import com.weather.forecast.model.Weather;
import com.weather.forecast.repository.CityRepository;
import com.weather.forecast.repository.CountryRepository;
import com.weather.forecast.repository.WeatherRepository;
import com.weather.forecast.service.WeatherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.hateoas.UriTemplate;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.Collections;

@Service
@Slf4j
public class WeatherServiceImpl implements WeatherService {

    private final WeatherRepository weatherRepository;
    private final CityRepository cityRepository;
    private final CountryRepository countryRepository;
    private static final String URL = "http://api.openweathermap.org/data/2.5/weather?q={city},{country}&APPID=1e0cea9c960a6a6851887dab736b3008";

    @Autowired
    public WeatherServiceImpl(WeatherRepository weatherRepository, CityRepository cityRepository,
                              CountryRepository countryRepository) {
        this.weatherRepository = weatherRepository;
        this.cityRepository = cityRepository;
        this.countryRepository = countryRepository;
    }

    @Override
    public void addWeather() {
        cityRepository.findAll().forEach(this::addLine);
    }

    private void addLine(City city1) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        //    httpHeaders.set("key", "value");
        HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);
        Country country = countryRepository.findById(city1.getCountryId()).orElseThrow(EntityNotFoundException::new);

        URI url = new UriTemplate(URL).expand(city1.getName(), country);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
        String forecast = responseEntity.getBody();

        ObjectMapper objectMapper = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addDeserializer(Weather.class, new Deserializer());
        objectMapper.registerModule(simpleModule);

        try {
            Weather weather = objectMapper.readValue(forecast, Weather.class);
            weather.setCity(city1);
            weatherRepository.save(weather);
        } catch (IOException e) {
            log.trace("Can`t parse object");
        }
    }
}
